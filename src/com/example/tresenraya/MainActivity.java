package com.example.tresenraya;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private int[] tablero = new int[9];
	private int jugadores = 0;
	private int turno = 1;
	private boolean ganar = false;
	int ganador = 0;
	private Listener radio;
	private ListenerMovs movimientos;
	TextView texto;
	Button b1, b2, b3, b4, b5, b6, b7, b8, b9, reiniciar, j1, j2;

	public void ganar() {

		if ((tablero[0] == 1) & (tablero[1] == 1) & (tablero[2] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[0] == 1) & (tablero[4] == 1) & (tablero[8] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[0] == 1) & (tablero[3] == 1) & (tablero[6] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[2] == 1) & (tablero[5] == 1) & (tablero[8] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[1] == 1) & (tablero[4] == 1) & (tablero[7] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[3] == 1) & (tablero[4] == 1) & (tablero[5] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[2] == 1) & (tablero[4] == 1) & (tablero[6] == 1)) {
			ganar = true;
			ganador = 1;
		} else if ((tablero[6] == 1) & (tablero[7] == 1) & (tablero[8] == 1)){
			ganar=true;
			ganador=1;
		} else if ((tablero[0] == 2) & (tablero[1] == 2) & (tablero[2] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[0] == 2) & (tablero[4] == 2) & (tablero[8] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[0] == 2) & (tablero[3] == 2) & (tablero[6] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[2] == 2) & (tablero[5] == 2) & (tablero[8] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[1] == 2) & (tablero[4] == 2) & (tablero[7] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[3] == 2) & (tablero[4] == 2) & (tablero[5] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[2] == 2) & (tablero[4] == 2) & (tablero[6] == 2)) {
			ganar = true;
			ganador = 2;
		} else if ((tablero[6] == 2) & (tablero[7] == 2) & (tablero[8] == 2)){
			ganar=true;
			ganador=2;
		} else if ((tablero[0]!=0) & (tablero[1]!=0) & (tablero[2]!=0) & (tablero[3]!=0) & (tablero[4]!=0) & (tablero[5]!=0) & (tablero[6]!=0) & (tablero[7]!=0) & (tablero[8]!=0)){
			ganador=3;
			ganar=true;
		}
		if ((ganador == 1) & (ganar == true)) {
			texto.setText("GANA  X");
		} else if ((ganador == 2) & (ganar == true)) {
			texto.setText("GANAN  O");
		} else if (ganador==3)
			texto.setText("EMPATE");
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public boolean getGanar() {
		return ganar;
	}

	public void setGanar(boolean ganar) {
		this.ganar = ganar;
	}

	public int getTurno() {
		return this.turno;
	}

	public void setJugadores(int num) {
		this.jugadores = num;
	}

	public int getJugadores() {
		return jugadores;
	}

	public void setPosicion(int pos, int jug) {

		tablero[pos] = jug;

	}

	public int getPosicion(int pos) {

		return tablero[pos];
	}

	public void reiniciar() {

		b1.setText("-");
		b2.setText("-");
		b3.setText("-");
		b4.setText("-");
		b5.setText("-");
		b6.setText("-");
		b7.setText("-");
		b8.setText("-");
		b9.setText("-");

		jugadores = 0;
		texto.setText("");
		ganador = 0;
		turno = 1;
		ganar = false;
		tablero[0] = 0;
		tablero[1] = 0;
		tablero[2] = 0;
		tablero[3] = 0;
		tablero[4] = 0;
		tablero[5] = 0;
		tablero[6] = 0;
		tablero[7] = 0;
		tablero[8] = 0;
		j1.setClickable(true);
		j2.setClickable(true);
	}

	public void desactivar() {
		j1.setClickable(false);
		j2.setClickable(false);
	}

	public void movMaquina() {
		if (ganar==false){
		int numero = 0;
		while (turno == 2) {
			numero = (int) (Math.random() * (8 + 1));
			if (tablero[numero] == 0) {
				tablero[numero] = 2;
				turno = 1;
			}
		}
		switch (numero) {
		case 0:
			b1.setText("O");
			break;
		case 1:
			b2.setText("O");
			break;
		case 2:
			b3.setText("O");
			break;
		case 3:
			b4.setText("O");
			break;
		case 4:
			b5.setText("O");
			break;
		case 5:
			b6.setText("O");
			break;
		case 6:
			b7.setText("O");
			break;
		case 7:
			b8.setText("O");
			break;
		case 8:
			b9.setText("O");
			break;

		}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		movimientos = new ListenerMovs(this);
		radio = new Listener(this);

		b1 = (Button) findViewById(R.id.boton1);
		b2 = (Button) findViewById(R.id.boton2);
		b3 = (Button) findViewById(R.id.boton3);
		b4 = (Button) findViewById(R.id.boton4);
		b5 = (Button) findViewById(R.id.boton5);
		b6 = (Button) findViewById(R.id.boton6);
		b7 = (Button) findViewById(R.id.boton7);
		b8 = (Button) findViewById(R.id.boton8);
		b9 = (Button) findViewById(R.id.boton9);
		reiniciar = (Button) findViewById(R.id.reinicar);

		b1.setOnClickListener(movimientos);
		b2.setOnClickListener(movimientos);
		b3.setOnClickListener(movimientos);
		b4.setOnClickListener(movimientos);
		b5.setOnClickListener(movimientos);
		b6.setOnClickListener(movimientos);
		b7.setOnClickListener(movimientos);
		b8.setOnClickListener(movimientos);
		b9.setOnClickListener(movimientos);

		texto = (TextView) findViewById(R.id.ganador);
		j1 = (Button) findViewById(R.id.solo);
		j2 = (Button) findViewById(R.id.multi);
		reiniciar.setOnClickListener(radio);
		j1.setOnClickListener(radio);
		j2.setOnClickListener(radio);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
