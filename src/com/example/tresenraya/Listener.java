package com.example.tresenraya;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Listener implements OnClickListener {

	private MainActivity a;

	public Listener(MainActivity a) {
		this.a = a;
	}

	@Override
	public void onClick(View v) {

		Button b = (Button) v;

		switch (b.getId()) {

		case R.id.solo:
			a.setJugadores(1);
			a.desactivar();

			break;

		case R.id.multi:
			a.setJugadores(2);
			a.desactivar();

			break;

		case R.id.reinicar:
			if (a.getGanar() == true){
				a.reiniciar();

				
			}
		}

	}

}