package com.example.tresenraya;


import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ListenerMovs implements OnClickListener {

	private MainActivity a;

	public ListenerMovs(MainActivity a) {
		this.a = a;
	}

	@Override
	public void onClick(View v) {

		Button b = (Button) v;

		if ((a.getJugadores() == 2) & (a.getGanar() == false)) {

			switch (b.getId()) {

			case R.id.boton1:
				if ((a.getPosicion(0) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(0, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(0) == 0) {
						a.setPosicion(0, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton2:
				if ((a.getPosicion(1) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(1, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(1) == 0) {
						a.setPosicion(1, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton3:
				if ((a.getPosicion(2) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(2, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(2) == 0) {

						a.setPosicion(2, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton4:
				if ((a.getPosicion(3) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(3, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(3) == 0) {

						a.setPosicion(3, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton5:
				if ((a.getPosicion(4) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(4, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(4) == 0) {

						a.setPosicion(4, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton6:
				if ((a.getPosicion(5) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(5, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(5) == 0) {

						a.setPosicion(5, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton7:
				if ((a.getPosicion(6) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(6, 1);
					a.setTurno(2);
					b.setText("X");
				}

				else {
					if (a.getPosicion(6) == 0) {

						a.setPosicion(6, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton8:
				if ((a.getPosicion(7) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(7, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(7) == 0) {

						a.setPosicion(7, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;

			case R.id.boton9:
				if ((a.getPosicion(8) == 0) & (a.getTurno() == 1)) {
					a.setPosicion(8, 1);
					a.setTurno(2);
					b.setText("X");
				} else {
					if (a.getPosicion(8) == 0) {

						a.setPosicion(8, 2);
						a.setTurno(1);
						b.setText("O");
					}
				}
				break;
			}
			a.ganar();
		} else

		if ((a.getJugadores() == 1) & (a.getGanar() == false)) {

			if (a.getTurno() == 1) {

				switch (b.getId()) {

				case R.id.boton1:
					if (a.getPosicion(0) == 0) {
						a.setPosicion(0, 1);
						a.setTurno(2);
						b.setText("X");
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0)) & a.getGanar() == false) {
							a.ganar();
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton2:
					if (a.getPosicion(1) == 0) {
						a.setPosicion(1, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0)) & a.getGanar() == false) {
							
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton3:
					if (a.getPosicion(2) == 0) {
						a.setPosicion(2, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton4:
					if (a.getPosicion(3) == 0) {
						a.setPosicion(3, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton5:
					if (a.getPosicion(4) == 0) {
						a.setPosicion(4, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton6:
					
					if (a.getPosicion(5) == 0) {
						a.setPosicion(5, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton7:
					if (a.getPosicion(6) == 0) {
						a.setPosicion(6, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton8:
					if (a.getPosicion(7) == 0) {
						a.setPosicion(7, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				case R.id.boton9:
					if (a.getPosicion(8) == 0) {
						a.setPosicion(8, 1);
						a.setTurno(2);
						b.setText("X");
						a.ganar();
						if (((a.getPosicion(0) == 0) || (a.getPosicion(1) == 0)
								|| (a.getPosicion(2) == 0)
								|| (a.getPosicion(3) == 0)
								|| (a.getPosicion(4) == 0)
								|| (a.getPosicion(5) == 0)
								|| (a.getPosicion(6) == 0)
								|| (a.getPosicion(7) == 0) || (a.getPosicion(8) == 0))
								& a.getGanar() == false) {
							a.movMaquina();
							a.ganar();
						}
					}
					break;

				}
			}
			a.ganar();

		}

	}

}
